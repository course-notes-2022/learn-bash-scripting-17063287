# 01: What is Bash?

Bash is a widely used shell on Linux systems. A **shell** is a program that allows us to interact with a computer.

Bash is short for "Bourne-again Shell", in reference to the earlier Bourne shell.

Bash scripts allow us to create **files** incorporating commands, logic, etc. that we can run and share with others. Combining commands into scripts saves time and reduces errors.

Bash is best for writing small to med size scripts that will run on Linux systems.

Bash is pre-installed or available on most linux systems. To check your bash version:

- `bash --version`

To check the default shell for your system:

- `echo $SHELL`

To change the shell to bash:

- `chsh bash`

Bash is primarily used on linux systems, but is available on macOS and Windows systems. Note that the version of Bash on macOS is extremely outdated and may be removed in the future. You can upgrade the version of bash using 3rd-party package managers like homebrew.
