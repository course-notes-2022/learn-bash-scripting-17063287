# Pipes and Redirections

**Pipes** send the output of one process/program to **another program**.

- `cat filename.txt | less`: Pipe the output of filename.txt to the `less`
  program.

We can stream many outputs to programs using pipes.

**Redirection** sends streams (standard input, output, and error) to or from
**files**:

| Stream | Name                     | Content                 |
| ------ | ------------------------ | ----------------------- |
| 0      | Standard input (stdin)   | Keyboard or other input |
| 1      | Standard output (stdout) | Regular output          |
| 2      | Standard error (stderr)  | Ouput marked as `error` |

| Symbol | Function                                                     |
| ------ | ------------------------------------------------------------ |
| `>`    | Output redirection (truncate, or overwrite destination file) |
| `>>`   | Output redirection (append to end of destination file)       |
| `<`    | Input redirection                                            |
| `<<`   | Here document                                                |

Note that you can redirect from a **specific stream** by adding the **stream
number** before the `>`/`>>` signs (`1` is assumed unless specified):

- `ls /fakedir 2> error.txt`: Redirect the output of `stderr` stream to
  `error.txt`.

**Input redirection** takes information **from** a file and provides it **to** a
standard input process:

- `cat < list.txt`

**Here documents** let us specify input **up to a specified string inside a
document**:

`limit-doc.sh`:

```
cat << EndOfText
This is a
multiline
text string
EndOfText
```

- `./limit-doc.sh`: run the `limit-doc.sh` file

**Output**:

```
This is a
multiline
text string

```
