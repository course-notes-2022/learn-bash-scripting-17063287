# Bash Builtin Commands

Many of the programs we use in Bash are **commands** that are separate from
Bash.

Bash includes builtin commands that are part of Bash itself:

- `echo`: Outputs text to stdout with a newline character
- `printf`: Outputs text to stdout with a newline character

## Builtins vs. Commands

Some programs have both builtin and command versions. We can explicitly run each
using the appropriate keyword:

- `command echo hello`: Run the 'command' version of `echo`
- `builtin echo hello`: Run the 'builtin' version of `echo`

To tell whether a program is a command or builtin:

- `command -V {command name}`
- `
