# Expansions and Substitutions

**Expansions and substitutions** allow us to specify values that aren't known
until a script runs, e.g. the results of a calculation, the path to the user's
home folder, etc.

| Representation | Name                 | Function                                                           |
| -------------- | -------------------- | ------------------------------------------------------------------ |
| `~`            | Tilde expansion      | Represents the user's `$HOME` environment variable/user's home dir |
| `{...}`        | Brace expansion      |                                                                    |
| `${...}`       | Parameter expansion  |                                                                    |
| `$(...)`       | Command substitution |                                                                    |
| `$((...))`     | Arithmetic expansion |                                                                    |

- `echo ~`: echo the current user's home directory location
