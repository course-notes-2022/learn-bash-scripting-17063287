# Brace Expansion

Brace expansion is written around an expression or list of values. It lets us
substitute items from a **comma-separated list of values**, or **ranges of
numbers/letters separated by `..`**:

- `{a,b,c}`
- `{x..y..i}`

| Representation | Name                 | Function                                                           |
| -------------- | -------------------- | ------------------------------------------------------------------ |
| `~`            | Tilde expansion      | Represents the user's `$HOME` environment variable/user's home dir |
| `{...}`        | Brace expansion      | Creates sets or ranges                                             |
| `${...}`       | Parameter expansion  |                                                                    |
| `$(...)`       | Command substitution |                                                                    |
| `$((...))`     | Arithmetic expansion |                                                                    |

## Examples

- `echo /tmp/{one,two,three}/file.txt`:

**Output**:

```
/tmp/one/file.txt /tmp/two/file.txt /tmp/three/file.txt
```

- `echo c{a,o,u}t`:

```
cat cot cut
```

- `echo {00..100}`:

```
000 001 002 003 004 005 006 007 008 009 010 011 012 013 014 015 016 017 018 019 020 021 022 023 024 025 026 027 028 029 030 031 032 033 034 035 036 037 038 039 040 041 042 043 044 045 046 047 048 049 050 051 052 053 054 055 056 057 058 059 060 061 062 063 064 065 066 067 068 069 070 071 072 073 074 075 076 077 078 079 080 081 082 083 084 085 086 087 088 089 090 091 092 093 094 095 096 097 098 099 100
```

- `echo {a..z..2}`: Echo every **other** letter between a and z

- `touch file_{01..06}{a..d}`: Generate a set of files from `file_01a.txt` to
  `file_06d.txt`

- `head -n1 {dir1, dir2, dir3}/lorem.txt`
