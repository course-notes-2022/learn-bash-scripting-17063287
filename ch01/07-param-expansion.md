# Parameter Expansion

| Representation | Name                 | Function                                                           |
| -------------- | -------------------- | ------------------------------------------------------------------ |
| `~`            | Tilde expansion      | Represents the user's `$HOME` environment variable/user's home dir |
| `{...}`        | Brace expansion      | Creates sets or ranges                                             |
| `${...}`       | Parameter expansion  | Retrieves and transforms stored values                             |
| `$(...)`       | Command substitution |                                                                    |
| `$((...))`     | Arithmetic expansion |                                                                    |

## Examples

```sh

a="Hello World"

echo $a # Hello World

echo ${a} # Hello World

echo ${a:1:9} # ello Worl

echo ${a/World/Everybody} # Hello Everybody

greeting = "hello there!"

# Use // before the search param to replace ALL occurences
echo ${greeting//e/_} # h_llo th_r_!
```
