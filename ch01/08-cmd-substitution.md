# Command Substitution

| Representation | Name                 | Function                                                           |
| -------------- | -------------------- | ------------------------------------------------------------------ |
| `~`            | Tilde expansion      | Represents the user's `$HOME` environment variable/user's home dir |
| `{...}`        | Brace expansion      | Creates sets or ranges                                             |
| `${...}`       | Parameter expansion  | Retrieves and transforms stored values                             |
| `$(...)`       | Command substitution | Puts the output of one command inside another                      |
| `$((...))`     | Arithmetic expansion |                                                                    |

```sh

uname -r # Get release version of OS kernel

echo "The kernel is $(uname -r)" # The kernel is 5.4.0-1086-azure
```
