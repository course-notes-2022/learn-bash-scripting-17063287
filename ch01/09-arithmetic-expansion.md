# Arithmetic Expansion

| Representation | Name                 | Function                                                           |
| -------------- | -------------------- | ------------------------------------------------------------------ |
| `~`            | Tilde expansion      | Represents the user's `$HOME` environment variable/user's home dir |
| `{...}`        | Brace expansion      | Creates sets or ranges                                             |
| `${...}`       | Parameter expansion  | Retrieves and transforms stored values                             |
| `$(...)`       | Command substitution | Puts the output of one command inside another                      |
| `$((...))`     | Arithmetic expansion | Performs arithmetic calculations                                   |

## Examples

```sh
echo $((2 + 2)) # 4

echo $((4 * 5)) # 20

echo $((4 / 5)) # 0; Bash can perform calculations only with integers
``
```
