# Understanding Bash Script Syntax

There are two ways to collect commands in a reusable format:

1. **one-liners**:

   - Many commands presented in one line of text
   - often piped commands or commands separated by semicolon
   - can be long (and even wrap), but no newline until the end
   - often stored in a note for frequent use

2. **Bash scripts**:

   - text file that contains a series of commands
   - `bash myscript.sh`
   - Executable bash script:

     - Includes a shebang as eh first line, followed by the full path to the
       program that should run the script
     - best practice is to write shebang line in a way that uses the environment
       to locate the executable

     ```sh
     #!/usr/bin/env bash
     ```

## Basic Script Syntax

```sh
#!/usr/bin/env bash

echo "hello"

# this is a comment

echo "there"

```

### Making a Script Executable

- `chmod +x myscript`

Run locally with `./mscript`, or `myscript` if in the `$PATH`.
